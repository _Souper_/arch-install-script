#!/bin/bash

clear
echo Boot mode: $1
echo Install disk: $2
echo What hostname do you want?
read hostn
echo $hostn > /etc/hostname
echo What username do you want? 
read username
useradd -m -G wheel,users,video $username
echo Set password for user:
passwd $username
echo Set password for root
passwd

if [[ $1 = "UEFI" ]]
then
    echo Installing grub for UEFI
    grub-install --efi-directory=/boot --removable --bootloader-id="Arch Linux"
else
    echo Installing grub for BIOS
    grub-install $2
fi

grub-mkconfig -o /boot/grub/grub.cfg

# Fix networking

systemctl disable dhcpcd
systemctl disable netctl
systemctl disable wpa_supplicant
systemctl enable NetworkManager
systemctl enable ntpd
systemctl enable sddm

# Locale, for now assume en_US

sync
LANGUAGE=en_US

sed 's/^#'$LANGUAGE'/'$LANGUAGE/ /etc/locale.gen > /tmp/locale
mv /tmp/locale /etc/locale.gen

echo "LANG=en_US.UTF-8" > /etc/locale.conf

locale-gen

echo "Timezone(Example: Europe/London)?"
read timezone

ln -sf /usr/share/zoneinfo/$timezone /etc/localtime

sed -i '/%wheel ALL=(ALL) ALL/s/^# //g' /etc/sudoers
