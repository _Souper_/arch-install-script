#!/bin/bash

lsblk

[ -d /sys/firmware/efi ] && bootmode="UEFI" || bootmode="BIOS"

echo Booted in $bootmode

echo What disk to install to? Entire disk will be erased and automatically partitioned. Write the full path so /dev/whatever: 
read installdisk

echo Installing to $installdisk
read -p "Are you sure? " -n 1 -r
echo
if ! [[ $REPLY =~ ^[Yy]$ ]]
then
    echo Got a negative response. Aborting.
    exit
fi
echo Partitioning $installdisk

if [[ $bootmode = "UEFI" ]]
then
    echo Creating new gpt partition table and making a 550MB EFI partition.
    parted $installdisk mktable gpt
    parted $installdisk mkpart ESP fat32 1MiB 551MiB
    parted $installdisk set 1 esp on
    parted $installdisk mkpart primary ext4 551MiB 100%

    sync

    ONE=1
    TWO=2

    DISK1=$installdisk$ONE
    DISK2=$installdisk$TWO

    mkfs.vfat -F32 $DISK1
    mkfs.ext4 $DISK2

    mount $DISK2 /mnt
    mkdir /mnt/boot
    mount $DISK1 /mnt/boot
else
    echo Creating new msdos partition table and filling partition with root.
    parted $installdisk mktable msdos
    parted $installdisk mkpart primary ext4 1MiB 100%
    parted $installdisk set 1 boot on

    sync

    ONE=1

    DISK1=$installdisk$ONE

    mkfs.ext4 $DISK1

    mount $DISK1 /mnt
fi

clear
echo "Additional packages to install? Separated by space, of course. (Your DE/Graphics card drivers/Web browser): "
read addstuff
pacstrap /mnt base base-devel $addstuff ntp efibootmgr gnome-keyring grub sddm ttf-dejavu git wget curl sudo dhcpcd wpa_supplicant dialog networkmanager network-manager-applet xterm
echo Pacstrapped, running post-install script.
wget https://gitlab.com/_Souper_/arch-install-script/raw/master/installphase2.sh -O /mnt/root/installphase2.sh
chmod +x /mnt/root/installphase2.sh
arch-chroot /mnt bash /root/installphase2.sh $bootmode $installdisk

sync

read -p "Installation finished. Unmount and reboot? " -n 1 -r
echo
if ! [[ $REPLY =~ ^[Yy]$ ]]
then
    genfstab -U /mnt >> /mnt/etc/fstab
    echo Got a negative response. Aborting.
    exit
fi

rm /mnt/root/installphase2.sh
genfstab -U /mnt >> /mnt/etc/fstab

sync
umount -R /mnt
reboot